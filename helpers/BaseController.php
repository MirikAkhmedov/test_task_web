<?php

namespace Core;
class BaseController
{
    public function view(string $name): string
    {
        return require dirname(__DIR__) . '/public/view/' . $name . '.php';
    }
}